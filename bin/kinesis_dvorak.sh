#!/bin/bash

name="Kinesis Advantage2 Keyboard"
ids="$(xinput --list | grep "\[slave  keyboard" | grep "$name" | egrep -o 'id=([0-9]+)' | cut -d= -f 2)"

for id in ${ids[@]}; do
    setxkbmap \
        -device "$id" \
        -layout us \
        -variant dvp \
        -option keypad:pointerkeys
done

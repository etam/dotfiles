#!/bin/bash

mountpoint -q ~/Nextcloud_crypt/ && exit 0

exec gocryptfs -extpass="pass misc/nextcloud_crypt2" ~/Nextcloud2/crypt/ ~/Nextcloud_crypt/

;; http://cachestocaches.com/2015/8/getting-started-use-package/
;; https://robert.kra.hn/posts/rust-emacs-setup/

(add-hook 'after-init-hook
          `(lambda ()
             (setq gc-cons-threshold ,gc-cons-threshold))
          'append)
(setq gc-cons-threshold most-positive-fixnum)

(setq custom-file "~/.emacs.d/init-custom.el")
(load "~/.emacs.d/init-custom.el" t t)

(require 'package)
(setq package-user-dir (locate-user-emacs-file
                        (concat
                         (file-name-as-directory "elpa")
                         emacs-version)))
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(setq package-enable-at-startup nil)
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)
(use-package diminish :ensure t :defer t)
(use-package bind-key :ensure t :defer t)


(use-package gnu-elpa-keyring-update :ensure t)

(use-package ace-window
  :ensure t
  :bind ("C-x o" . ace-window)
  :custom
  (aw-keys '(?a ?o ?e ?u ?i ?d ?h ?t ?n)) ;; dvorak
  )

(use-package ag
  :ensure t
  )

(use-package anaconda-mode
  :ensure t
  :init
  (add-hook 'python-mode-hook 'anaconda-mode)
  (add-hook 'python-mode-hook 'anaconda-eldoc-mode)
  )

(use-package company-anaconda
  :ensure t
  :after (anaconda-mode company)
  :config
  (add-to-list 'company-backends 'company-anaconda)
  )

(use-package beacon
  :ensure t
  :config (beacon-mode 1))

(use-package bm
  :ensure t
  )

(use-package buffer-move
  :ensure t
  :bind (("<C-S-up>"    . buf-move-up)
         ("<C-S-down>"  . buf-move-down)
         ("<C-S-left>"  . buf-move-left)
         ("<C-S-right>" . buf-move-right))
  :custom
  (buffer-move-behavior 'move)
  )

(use-package company
  :ensure t
  :bind ("M-RET" . company-complete)
  :custom
  (company-idle-delay 0.5)
  (company-minimum-prefix-length 1)
  (company-dabbrev-downcase nil)
  (company-dabbrev-ignore-case nil)
  (company-tooltip-align-annotations t)
  :config
  (global-company-mode)
  :diminish company-mode
  )

(use-package counsel
  :ensure t
  :demand
  :bind (("M-x"     . counsel-M-x)
         ("M-y"     . counsel-yank-pop)
         ("C-x C-f" . counsel-find-file)
         ("C-h f"   . counsel-describe-function)
         ("C-h v"   . counsel-describe-variable))
  )

(use-package flycheck
  :ensure t
  :config
  (global-flycheck-mode)
  )

(use-package flycheck-pos-tip
  :ensure t
  :after (flycheck)
  :config
  (flycheck-pos-tip-mode))

;; (use-package forge
;;   :ensure t
;;   :after magit
;;   :custom
;;   (auth-sources '("~/.authinfo.gpg"))
;;   )

(use-package git-link
  :ensure t
  :custom
  (git-link-use-commit t)
  )

(use-package ibuffer
  :bind ("C-x C-b" . ibuffer)
  )

(use-package ivy
  :ensure t
  :demand
  :diminish ivy-mode
  :commands (ivy-completing-read)
  :bind ("C-c C-r" . ivy-resume)
  :custom
  (ivy-count-format "(%d/%d) ")
  (ivy-fixed-height-minibuffer t)
  (ivy-use-selectable-prompt t)
  (magit-completing-read-function 'ivy-completing-read)
  :config
  (ivy-mode 1)
  )

(use-package lsp-mode
  :ensure t
  :commands lsp
  :hook (
         (rust-mode . lsp)
         )
  :custom
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  (lsp-eldoc-render-all t)
  (lsp-idle-delay 0.6)
  (lsp-enable-snippet nil)
  :init
  (add-hook 'lsp-mode-hook 'lsp-ui-mode)
  (add-hook 'c-mode-hook 'lsp)
  (add-hook 'c++-mode-hook 'lsp)
  )

(use-package lsp-ivy
  :ensure t
  :commands lsp-ivy-workspace-symbol
  )

(use-package lsp-ui
  :ensure t
  :commands lsp-ui-mode
  :custom
  (lsp-ui-peek-always-show t)
  (lsp-ui-sideline-show-hover t)
  (lsp-ui-doc-enable nil)
  )

(use-package magit
  :ensure t
  :bind ("C-x g" . magit-status)
  :functions (magit-add-section-hook)
  :custom
  (magit-commit-arguments nil)
  (magit-fetch-arguments '("--prune"))
  (magit-process-popup-time 0)
  (magit-rebase-arguments '("--autosquash"))
  (magit-tag-arguments '("--sign"))
  :config
  (magit-add-section-hook 'magit-status-sections-hook 'magit-insert-modules-overview nil t)
  )

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :custom
  (markdown-command "multimarkdown")
  )

(use-package meson-mode
  :ensure t
  :mode "\\meson\\.build\\'"
  :init
  (add-hook 'meson-mode-hook 'company-mode)
  )

(use-package projectile
  :ensure t
  :custom
  (projectile-completion-system 'ivy)
  )

(use-package rustic
  :ensure
  :bind (:map rustic-mode-map
              ("M-j" . lsp-ui-imenu)
              ("M-?" . lsp-find-references))
  :init
  (add-hook 'rustic-mode-hook 'rk/rustic-mode-hook))

(defun rk/rustic-mode-hook ()
  ;; so that run C-c C-c C-r works without having to confirm, but don't try to
  ;; save rust buffers that are not file visiting. Once
  ;; https://github.com/brotzeit/rustic/issues/253 has been resolved this should
  ;; no longer be necessary.
  (when buffer-file-name
    (setq-local buffer-save-without-query t))
  (add-hook 'before-save-hook 'lsp-format-buffer nil t))

(use-package flycheck-rust
  :ensure t
  :init
  (add-hook 'flycheck-mode-hook #'flycheck-rust-setup)
  )

(use-package smex
  :ensure t
  :custom
  (smex-save-file "~/.emacs.d/.smex-items")
  )

(use-package swiper
  :ensure t
  :bind ("C-s" . swiper)
  )

(use-package undo-tree
  :ensure t
  :diminish undo-tree-mode
  :config
  (global-undo-tree-mode)
  )

(use-package visual-regexp
  :ensure t
  :commands (vr/replace vr/query-replace)
  )

(use-package vterm)

(use-package wgrep
  :ensure t
  )

(use-package wgrep-ag
  :ensure t
  :init
  (add-hook 'ag-mode-hook 'wgrep-ag-setup)
  )

(use-package yaml-mode
  :ensure t
  :init
  (add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode))
  )

(use-package ztree
  :ensure t
  :commands (ztree-diff ztree-dir)
  )


(winner-mode 1)
(add-hook 'ediff-after-quit-hook-internal 'winner-undo)


(global-set-key (kbd "C-x k") 'kill-this-buffer)
(global-set-key (kbd "C-x C-d")
  (lambda () (interactive)
    (dired default-directory)))

(defun server-start-named (name)
  "Start server with NAME."
  (interactive "sname: ")
  (setq server-name name)
  (setenv "EDITOR" (concat "emacsclient -s " name))
  (server-start)
  )

;; http://stackoverflow.com/a/38655619/952263
;; https://github.com/ffevotte/desktop-plus/blob/master/desktop%2B.el
(defun session-start (session-name)
  ;;(interactive)
  (setq desktop-dirname (concat "~/.emacs.d/sessions/" session-name "/"))
  (setq desktop-path (list desktop-dirname))

  (if (file-directory-p desktop-dirname)
      (desktop-read desktop-dirname)
    (progn
      (make-directory desktop-dirname t)
      (desktop-save desktop-dirname)))
  (desktop-save-mode 1)

  (setq bookmark-default-file (concat desktop-dirname "bookmarks"))
  (let ((backup-dir (concat desktop-dirname "backup")))
    (setq backup-directory-alist (list (cons "." backup-dir)))
    )

  ;;(setq frame-title-format '("" "%b @ " session-name))

  (server-start-named session-name)
  )

;; http://stackoverflow.com/a/18317181/952263
(defun my-find-file-check-make-large-file-read-only-hook ()
  "If a file is over a given size, make the buffer read only."
  (when (> (buffer-size) (* 1024 1024))
    (setq buffer-read-only t)
    (buffer-disable-undo)
    (setq bidi-display-reordering nil)
    (jit-lock-mode nil)
    (fundamental-mode)))
(add-hook 'find-file-hook 'my-find-file-check-make-large-file-read-only-hook)

(put 'scroll-left 'disabled nil)

;; https://superuser.com/a/349997/99610
(global-unset-key (kbd "C-z"))
(put 'upcase-region 'disabled nil)

(menu-bar-mode -1)

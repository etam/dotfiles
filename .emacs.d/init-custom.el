(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(async-bytecomp-package-mode t)
 '(auto-revert-verbose nil)
 '(backup-by-copying t)
 '(backup-directory-alist '(("." . "~/.emacs.d/backup")))
 '(c-default-style '((c-mode . "stroustrup") (c++-mode . "stroustrup")))
 '(c-offsets-alist '((innamespace . +)))
 '(column-number-mode t)
 '(custom-enabled-themes '(deeper-blue))
 '(delete-selection-mode t)
 '(dired-dwim-target t)
 '(ediff-split-window-function 'split-window-horizontally)
 '(ediff-window-setup-function 'ediff-setup-windows-plain)
 '(eldoc-echo-area-prefer-doc-buffer t)
 '(enable-recursive-minibuffers t)
 '(epg-pinentry-mode 'ask)
 '(gdb-many-windows t)
 '(gdb-show-main t)
 '(global-auto-revert-mode t)
 '(global-auto-revert-non-file-buffers t)
 '(indent-tabs-mode nil)
 '(inhibit-startup-screen t)
 '(ispell-program-name "hunspell")
 '(large-file-warning-threshold nil)
 '(package-enable-at-startup nil)
 '(package-selected-packages
   '(git-link rustic gnu-elpa-keyring-update forge company-lsp lsp-ui lsp-ivy lsp-mode flycheck-rust symbol-overlay racer diminish cargo rust-mode beacon meson-mode company-anaconda anaconda-mode bm wgrep-ag wgrep yaml-mode expand-region flycheck-pos-tip flycheck company buffer-move ztree visual-regexp markdown-mode ace-window ag counsel ivy magit projectile smex swiper undo-tree use-package))
 '(ring-bell-function 'ignore)
 '(scroll-conservatively 101)
 '(show-paren-mode t)
 '(split-height-threshold nil)
 '(split-width-threshold nil)
 '(tab-width 4)
 '(term-buffer-maximum-size 0)
 '(tool-bar-mode nil)
 '(tramp-default-method "sshx")
 '(truncate-lines t)
 '(undo-tree-history-directory-alist '(("." . "~/.emacs.d/undo")))
 '(vc-handled-backends nil)
 '(view-read-only t)
 '(warning-suppress-log-types '((comp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Source Code Pro" :foundry "ADBO" :slant normal :weight normal :height 90 :width normal)))))

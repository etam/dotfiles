case "$-" in
    *i*) is_interactive="true" ;;
esac

# Exports

# http://superuser.com/a/753948
pathprepend() {
    for ((i=$#; i>0; i--)); do
        ARG="${!i}"
        if [[ -d "$ARG" && ":$PATH:" != *":$ARG:"* ]]; then
            PATH="$ARG${PATH:+":$PATH"}"
        fi
    done
}
pathprepend ~/.local/bin ~/.cargo/bin ~/.local/share/flatpak/exports/bin/

[[ -z "$EDITOR" ]] && \
   export EDITOR=mcedit
export TERMINAL=konsole

export GIT_PS1_SHOWDIRTYSTATE=true
export GIT_PS1_SHOWSTASHSTATE=true
export GIT_PS1_SHOWUNTRACKEDFILES=true
export GIT_PS1_SHOWUPSTREAM=auto

# curl
export IPFS_GATEWAY=http://datahouse2.wg:8080/

export WINEDLLOVERRIDES=winemenubuilder.exe=d
export WINEDEBUG=-all

export VAGRANT_DEFAULT_PROVIDER=libvirt

# history

export HISTCONTROL=ignoreboth
export HISTSIZE=100000
export HISTFILESIZE=$HISTSIZE
shopt -s histappend

# Interactive

if [[ "$is_interactive" == "true" ]]; then

    # Prompt

    __COLOR_DEFAULT='\[\e[0m\]'
    #__COLOR_BLACK='\[\e[0;30m\]'
    #__COLOR_RED='\[\e[0;31m\]'
    #__COLOR_GREEN='\[\e[0;32m\]'
    #__COLOR_BROWN='\[\e[0;33m\]'
    #__COLOR_BLUE='\[\e[0;34m\]'
    #__COLOR_PURPLE='\[\e[0;35m\]'
    __COLOR_CYAN='\[\e[0;36m\]'
    #__COLOR_GRAY='\[\e[0;37m\]'
    #__COLOR_DARK_GRAY='\[\e[1;30m\]'
    __COLOR_L_RED='\[\e[1;31m\]'
    __COLOR_L_GREEN='\[\e[1;32m\]'
    #__COLOR_YELLOW='\[\e[1;33m\]'
    __COLOR_L_BLUE='\[\e[1;34m\]'
    #__COLOR_L_PURPLE='\[\e[1;35m\]'
    #__COLOR_L_CYAN='\[\e[1;36m\]'
    #__COLOR_WHITE='\[\e[1;37m\]'

    __PS1_time="${__COLOR_CYAN}[\A]"
    __PS1_user_host="${__COLOR_L_GREEN}\u@\h"
    __PS1_path="${__COLOR_L_BLUE}:\w"

    __PS1_git_helper() {
        [[ -n "$PS1_NO_SCM" ]] || ! type __git_ps1 >/dev/null 2>&1 \
            && exit $1
        local -r exitstatus=$1
        __git_ps1
        exit $exitstatus
    }
    __PS1_git="${__COLOR_CYAN}\$(__PS1_git_helper \$?)"

    __PS1_err_code_condidtional() {
        (( $1 != 0 )) \
            && echo -ne " $1"
    }
    __PS1_err_code="${__COLOR_L_RED}\$(__PS1_err_code_condidtional \$?)"
    __PS1_end="${__COLOR_L_BLUE}>${__COLOR_DEFAULT} "

    PS1="${__PS1_time} ${__PS1_user_host}${__PS1_path}${__PS1_git}${__PS1_err_code}\n${__PS1_end}"

    # Aliases

    #alias la="ls -lah"
    alias la="eza -lgHaa --time-style long-iso"
    alias cat="bat --no-pager"

    # Misc

    cd() {
        if (( $# == 0 )); then
            builtin cd
        else
            builtin cd "$@"
            (( $? == 0 )) \
                && eza -lgHaa --time-style long-iso >&2
        fi
    }

    # disable ctrl+s/ctrl+q
    stty -ixon
fi
